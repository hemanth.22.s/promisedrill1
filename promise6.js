// 6. Write a funtion named `wait` that accepts `time` in ms and executes the 
// function after the given time.

function wait(time){
    setTimeout(()=>{
        console.log("Executed after: " + time + "ms");
    },time)
}

wait(2000)