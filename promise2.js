// 2. Create another promise. Now have it reject with a value of `Rejected Promise!` without 
// using `setTimeout`. Print the contents of the promise after it has been rejected by passing 
// console.log to `.catch`

let promise = new Promise((resolve, reject)=>{
    setTimeout(()=>{
        let task_performed = false;
        if(task_performed) {
            resolve('Promise Resolved.');
        } else {
            reject('Rejected Promise!');
        }
    }, 2 * 1000)
})
promise.then((resolve) => console.log(resolve))
.catch((reject) => console.log(reject));
