// 7. Write a basic implementation of `Promise.all` that accepts an array of promises and 
// return another array with the data coming from all the promises. Make sure if any of the 
// Promise gets rejected throw error. Only when all the promises are fulfilled resolve the promise.

const promise1 = new Promise((resolve,reject)=>{
    resolve(3)
})
const promise2 = new Promise((resolve, reject)=>{
    resolve(42)
    // reject("failed")
})
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo');
});

Promise.all([promise1, promise2, promise3])
.then((values) => {
  console.log(values);
})
.catch((values)=>{
    console.log(values);
})

