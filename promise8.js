// 8. Use this data endpoint to get the data and console the each house names and handle the error as well.
//   [ENDPOINT](https://raw.githubusercontent.com/nnnkit/json-data-collections/master/got-houses.json)

//     - Use fetch to get data.
//     - Handle if the user is not connected to internet.
//     - Handle error that may occure while fetching data.

// import fetch from "node-fetch";
const fetch = require('node-fetch')
// import { fetch } from "node-fetch";


fetch('https://raw.githubusercontent.com/nnnkit/json-data-collections/master/got-houses.json')
  .then((response) => response.json())
  .then((data) => console.log(data.houses.map((ele)=>{
    return ele.name
  })))
  .catch((err) =>console.log("User is not connected to internet"))
